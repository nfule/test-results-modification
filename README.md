# Test Results Modification

This script is basically for modifying results of failing tests, in the case that an intended change in the source code or filters also affects the expected test results.

## For OpeniT Use Only

For internal use only for OpeniT Development purposes! Needs an Altus setup and openit_testrunner binary. 

## How to Use

Note: Make sure that the script and the input text file are in the same directory.

Instructions:
1. Run your **component** or **integration** tests. 

**Example result:**

FAILED : 26 tests failed! (60 succeeded.)  (PT6M14.107S, avg 0.98 sec)
  - tests/component/licensestatusconverter/database/create_product_from_features-01
  - tests/component/licensestatusconverter/database/daemon_rename-01
  - tests/component/licensestatusconverter/database/daemon_rename-02
  - tests/component/licensestatusconverter/database/feature_4891/total_reserve_withdup1
  - tests/component/licensestatusconverter/database/feature_4891/total_reserve_withdup2
  - tests/component/licensestatusconverter/database/feature_4891/total_reserve_withmap
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/01_product_rename_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/02_feature_rename_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/03_product_mapping_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/04_feature_mapping_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/05_user_rename_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/06_host_rename_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/07_feature_rename_id_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/08_daemon_rename_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/09_license_mapping_diff_case
  - tests/component/licensestatusconverter/database/mapping_files_diff_casing/11_product_features_diff_case
  - tests/component/licensestatusconverter/database/product_mapping-01
  - tests/component/licensestatusconverter/database/product_mapping-02
  - tests/component/licensestatusconverter/database/product_mapping-03
  - tests/component/licensestatusconverter/database/product_mapping-04
  - tests/component/licensestatusconverter/database/redmine_15477
  - tests/component/licensestatusconverter/database/redmine_23327
  - tests/component/licensestatusconverter/database/redmine_2981
  - tests/component/licensestatusconverter/database/redmine_3377
  - tests/component/licensestatusconverter/database/redmine_3501
  - tests/component/licensestatusconverter/database/redmine_5166

2. Copy and paste this CMD result into the file **dummy.txt** and modify as format below

**Example input file content:**

- licensestatusconverter\database\create_product_from_features-01
- licensestatusconverter\database\daemon_rename-01
- licensestatusconverter\database\daemon_rename-02
- licensestatusconverter\database\feature_4891\total_reserve_withdup1
- licensestatusconverter\database\feature_4891\total_reserve_withdup2
- licensestatusconverter\database\feature_4891\total_reserve_withmap
- licensestatusconverter\database\mapping_files_diff_casing\01_product_rename_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\02_feature_rename_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\03_product_mapping_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\04_feature_mapping_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\05_user_rename_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\06_host_rename_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\07_feature_rename_id_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\08_daemon_rename_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\09_license_mapping_diff_case
- licensestatusconverter\database\mapping_files_diff_casing\11_product_features_diff_case
- licensestatusconverter\database\product_mapping-01
- licensestatusconverter\database\product_mapping-02
- licensestatusconverter\database\product_mapping-03
- licensestatusconverter\database\product_mapping-04
- licensestatusconverter\database\redmine_15477
- licensestatusconverter\database\redmine_23327
- licensestatusconverter\database\redmine_2981
- licensestatusconverter\database\redmine_3377
- licensestatusconverter\database\redmine_3501
- licensestatusconverter\database\redmine_5166



3. Modify **customized** variable value

**Example**

For licensestatusconverter tests, the test output for in the sandbox would be on **licensestatusconverter\database\mapping_files_diff_casing\01_product_rename_diff_case\data\database\license-status**. Hence, **customized** would be **data\database\license-status**

4. This script uses one argument which is the branch name you're using i.e. **fix_rm34153-9.16**. Hence, run the batch script **test_replace.bat fix_rm34153-9.16**

3. Run batch script. Enjoy saved time!
