@echo off
set arg1=%1
set customized=data\database\license-status

for /f "tokens=*" %%s in (dummy.txt) do (
	del /F /Q C:\levo-master\packages\altus\test\tests\component\%%s\out\*
	xcopy /s C:\levo-master\sandbox\altus_testrunner\%arg1%\simulated\component\%%s\%customized%\* C:\levo-master\packages\altus\test\tests\component\%%s\out\
)
pause
